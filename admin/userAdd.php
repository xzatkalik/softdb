<?php
// Include config file
require_once "connect.php";
 
// Define variables and initialize with empty values
$username = $password = $confirm_password = "";
$username_err = $password_err = $confirm_password_err = "";
 
// Processing form data when form is submitted
if($_SERVER["REQUEST_METHOD"] == "POST"){
 
    // Validate username
    if(empty(trim($_POST["username"]))){
        $username_err = "Prosim zadajte pouzivatelske meno";
    } else{
        // Prepare a select statement
        $sql = "SELECT PouzivatelID FROM pouzivatelia WHERE meno = ?";
        
        if($stmt = mysqli_prepare($mysqli, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "s", $param_username);
            
            // Set parameters
            $param_username = trim($_POST["username"]);
            
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                /* store result */
                mysqli_stmt_store_result($stmt);
                
                if(mysqli_stmt_num_rows($stmt) == 1){
                    $username_err = "Toto pouzivatelske meno je uz obsadene";
                } else{
                    $username = trim($_POST["username"]);
                }
            } else{
                echo "Oops! Skuskte neskor";
            }
        }
         
        // Close statement
        mysqli_stmt_close($stmt);
    }
    
    // Validate password
    if(empty(trim($_POST["password"]))){
        $password_err = "Prosim zadajte heslo";     
    } elseif(strlen(trim($_POST["password"])) < 6){
        $password_err = "Heslo musi mat aspon 6 znakov.";
    } else{
        $password = trim($_POST["password"]);
    }
    
    // Validate confirm password
    if(empty(trim($_POST["confirm_password"]))){
        $confirm_password_err = "Prosim potvrdte heslo";     
    } else{
        $confirm_password = trim($_POST["confirm_password"]);
        if(empty($password_err) && ($password != $confirm_password)){
            $confirm_password_err = "Hesla sa nezhoduju";
        }
    }
    
    // Check input errors before inserting in database
    if(empty($username_err) && empty($password_err) && empty($confirm_password_err)){
        
	 $param_username = $username;
         $param_password = password_hash($password, PASSWORD_DEFAULT); // Creates a password hash





        // Prepare an insert statement
        $sql = "INSERT INTO `pouzivatelia` (`meno`, `heslo`) VALUES ('$param_username', '$param_password')";
   
	 if($mysqli->query($sql) === TRUE){
                        echo "uspesne pridane";
                }else{
                        echo "chyba: ".$sql."<br>".$mysqli->error;
                }
 
	     
                
    }
    
    // Close connection
    mysqli_close($mysqli);
}
?>
 
<!DOCTYPE html>
<!--<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Sign Up</title>
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <style type="text/css">
        body{ font: 14px sans-serif; }
        .wrapper{ width: 350px; padding: 20px; }
    </style>
</head>
<body>-->
<div class="toolbox">
        <ul>
                <li><a href="index.php?stranka=user">Spat</a></li>
        </ul>

</div>
	


        <h2>Novy </h2>
        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>?stranka=userAdd" method="post">
            <div class="form-group <?php echo (!empty($username_err)) ? 'has-error' : ''; ?>">
                <label>Pouzivatelske meno:</label><br>
                <input type="text" name="username" class="form-control" value="<?php echo $username; ?>">
                <span class="error">* <?php echo $username_err; ?></span>
            </div>    
            <div class="form-group <?php echo (!empty($password_err)) ? 'has-error' : ''; ?>">
                <label>Heslo:</label><br>
                <input type="password" name="password" class="form-control" value="<?php echo $password; ?>">
                <span class="error">* <?php echo $password_err; ?></span>
            </div>
            <div class="form-group <?php echo (!empty($confirm_password_err)) ? 'has-error' : ''; ?>">
                <label>Zopakujte heslo:</label><br>
                <input type="password" name="confirm_password" class="form-control" value="<?php echo $confirm_password; ?>">
                <span class="error">* <?php echo $confirm_password_err; ?></span>
            </div>
		
            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="Pridaj">
                <input type="reset" class="btn btn-default" value="Vymaz">
        </form>
    </div>    
<!--</body>
</html>-->
