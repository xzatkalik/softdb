	<?php
	// Initialize the session
	session_start();
	 
	// Check if the user is logged in, if not then redirect him to login page
	if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
	    header("location: ../login.php");
	    exit;
	}
	?>

	<!DOCTYPE html>
	<html lang="sk-SK">
	<head>
		<meta charset="utf-8">
        <meta name="keywords" content="HTML, CSS">
        <meta name="description" content="Zaklady HTML a Css">
        <meta name="author" content="Domco na Lenovaku">
		<link rel="stylesheet" type"text/css" href="../style.css">
        <title> Software DB - databaza softveru</title>
</head>
<!-- =========telo html========== -->
<body>
<div class="obal">
<!-- <h1>Northwind DB</h1> -->
<header>
<div class="userinfo">
<b><?php echo $_SESSION['username']; ?> <br>
<hr>
<a href="index.php?stranka=logout">Odhlasit sa</a></b>
</div>
<a href="../index.php?stranka=domov"><img src="../images/logo.jpg" alt="logo-web"></a>
<h1>Databaza softveru [Admin mod]</h1>
</header>
<nav>
	<ul>

		<li><a href="index.php?stranka=home">Domov</a></li>
		<li><a href="index.php?stranka=users">Pouzivatelia</a></li>
		<li><a href="index.php?stranka=items">Polozky</a></li>
		<li><a href="index.php?stranka=category">Kategorie</a></li>
		<li><a href="index.php?stranka=platforms">Platforma</a></li>
		<li><a href="index.php?stranka=licences">Licencie</a></li>
	<!--	<li><a href="index.php?stranka=logout">Odhlasit sa</a></li> -->




	</ul>
</nav>
<article>


<?php
if (isset($_GET['stranka']))
        $stranka = $_GET['stranka'];
else
        $stranka = 'domov';

if (preg_match('/^[a-z0-9A-Z]+$/', $stranka))
{
        $vlozena = include('./' . $stranka . '.php');
        if (!$vlozena)
                echo('Podstránka nenajdena');
}
else
        echo('Neplatný parameter.');

?>
<!--  <hr> -->
</article>
<footer>

(C)2019-2021 Dominik Zatkalik

</footer>



</div>
</body>
</html>

