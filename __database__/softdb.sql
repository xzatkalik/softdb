-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 14, 2019 at 07:18 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `softdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `kategorie`
--

CREATE TABLE `kategorie` (
  `KategoriaID` int(11) NOT NULL,
  `KategoriaMeno` varchar(60) COLLATE utf8_bin NOT NULL,
  `Popis` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `kategorie`
--

INSERT INTO `kategorie` (`KategoriaID`, `KategoriaMeno`, `Popis`) VALUES
(1, 'Nezaradena kategoria', 'Nezaradene'),
(2, 'Multimedia', ''),
(3, 'CADx', 'Modelovanie '),
(4, 'Programovanie', 'Nastroje na programovanie'),
(5, 'Grafika', 'Vektorova a rastrova grafika'),
(6, 'Kancelaria', 'Kancelarsky sofrver'),
(7, 'Webove prehliadace', ''),
(8, 'Email', 'Emailove klienty, komunikacia'),
(9, 'Suborove systemy', 'Programy pre pracu so subormi');

-- --------------------------------------------------------

--
-- Table structure for table `licencie`
--

CREATE TABLE `licencie` (
  `LicencieID` int(11) NOT NULL,
  `Typ` varchar(60) COLLATE utf8_bin NOT NULL,
  `Poznamka` varchar(60) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `licencie`
--

INSERT INTO `licencie` (`LicencieID`, `Typ`, `Poznamka`) VALUES
(1, 'Nezaradena licencia', ''),
(2, 'Open Source', 'sds'),
(4, 'Freeware', 'volne dostupne'),
(5, 'Multi-Licencia', 'multilicencia pre potreby podniku'),
(6, 'Licencia pre jednotlivcov', 'Licencia na poziadanie Administratora'),
(7, 'Trial - Test', 'Testovacia verzia');

-- --------------------------------------------------------

--
-- Table structure for table `platforma`
--

CREATE TABLE `platforma` (
  `PlatformaID` int(11) NOT NULL,
  `Popis` varchar(60) COLLATE utf8_bin NOT NULL,
  `OS` varchar(60) COLLATE utf8_bin NOT NULL,
  `HW` varchar(60) COLLATE utf8_bin NOT NULL,
  `SW` varchar(60) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `platforma`
--

INSERT INTO `platforma` (`PlatformaID`, `Popis`, `OS`, `HW`, `SW`) VALUES
(1, 'Nezaradena platforma', '---', '---', '---'),
(2, 'Multiplatformova', 'Windows, LINUX, Mac', 'IBM-PC, Mac, x64, x86, ...', 'zakladne funkcie OS'),
(3, 'Windows Only', 'Windows', '---', '----'),
(4, 'Tex', '---', '---', 'Tex, Latex, MikTex, ...'),
(5, 'Linux', 'Linux', '---', 'GTK+ alebo QT kniznice'),
(6, 'PowerMac', 'Mac OS', 'procesor typu powerPC', '---'),
(7, 'STM', '---', 'MCU STM', '---');

-- --------------------------------------------------------

--
-- Table structure for table `polozky`
--

CREATE TABLE `polozky` (
  `PolozkyID` int(11) NOT NULL,
  `PolozkaMeno` varchar(60) COLLATE utf8_bin NOT NULL,
  `IDLicencia` int(11) NOT NULL,
  `IDKategoria` int(11) NOT NULL,
  `IDPlatforma` int(11) NOT NULL,
  `Homepage` varchar(60) COLLATE utf8_bin NOT NULL,
  `Tutorial` varchar(60) COLLATE utf8_bin NOT NULL,
  `Poznamka` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `polozky`
--

INSERT INTO `polozky` (`PolozkyID`, `PolozkaMeno`, `IDLicencia`, `IDKategoria`, `IDPlatforma`, `Homepage`, `Tutorial`, `Poznamka`) VALUES
(1, 'Safe Exam Browser', 1, 7, 2, 'https://safeexambrowser.org', '', 'Safe Exam Browser is a web browser environment to carry out e-assessments safely. '),
(2, 'GCC', 2, 4, 2, '', '', 'Kompilator pre jazyk C a C++'),
(4, 'Firefox', 2, 7, 2, 'https://www.mozilla.sk/firefox/', '', 'Firefox'),
(5, 'Tex Studio', 2, 6, 4, 'https://www.texstudio.org/', '', 'Prostredie pre Latex'),
(6, 'Texmaker', 2, 6, 4, 'http://www.xm1math.net/texmaker/', '', ''),
(7, 'Chromium', 2, 7, 2, 'https://www.chromium.org/', '', ''),
(8, 'WaterFox', 2, 7, 2, 'https://waterfoxproject.org/en-US/', '', 'Klon programu FireFox'),
(9, 'Opera', 1, 7, 2, 'https://www.opera.com/?utm_campaign=%2300%20-%20WW%20-%20Sea', '', ''),
(10, 'Libre Office', 2, 6, 2, 'https://sk.libreoffice.org/', '', ''),
(11, 'Open Office', 2, 6, 2, '', '', ''),
(12, 'MS Office 2019', 5, 6, 3, '', '', ''),
(13, 'Auto CAD', 6, 3, 3, '', '', ''),
(14, 'MatLab', 6, 4, 2, 'https://www.mathworks.com/products/matlab.html', '', ''),
(15, 'Thunderbird', 2, 8, 2, 'https://www.thunderbird.net/en-US/', '', ''),
(16, 'KMail', 2, 8, 5, '', '', ''),
(17, 'Win RAR', 7, 9, 3, 'https://www.win-rar.com/start.html?&L=0', '', ''),
(18, 'Double Commander', 2, 9, 2, 'https://doublecmd.sourceforge.io/', '', ''),
(19, 'Total Commander', 7, 9, 3, 'https://www.ghisler.com/', '', ''),
(20, 'Midnight Commander', 2, 9, 5, 'https://midnight-commander.org/', '', ''),
(21, 'Dolphin', 2, 9, 5, '', '', ''),
(22, 'GIMP', 2, 5, 2, 'https://www.gimp.org/', '', 'klon Photoshop'),
(23, 'Inkscape', 2, 5, 2, 'https://inkscape.org/', '', 'SVG, Vektorova grafika, editacia PDF'),
(24, 'Vlc player', 2, 2, 1, 'https://www.videolan.org/vlc/index.sk.html', '', ''),
(25, 'SMPlayer', 1, 2, 2, 'https://www.smplayer.info/', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `pouzivatelia`
--

CREATE TABLE `pouzivatelia` (
  `PouzivatelID` int(11) NOT NULL,
  `meno` varchar(50) COLLATE utf8_bin NOT NULL,
  `heslo` varchar(255) COLLATE utf8_bin NOT NULL,
  `vytvoreny` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `pouzivatelia`
--

INSERT INTO `pouzivatelia` (`PouzivatelID`, `meno`, `heslo`, `vytvoreny`) VALUES
(11, 'admin', '$2y$10$wip9g2gKTc9e136/0Sa6ROQf0SbmMjTzDaCWQdN5DAR96pS9X26lW', '2019-05-04 22:26:58'),
(12, 'adminko', '$2y$10$.se8PTBl/UuYNiMPvbkIlOa2ij9JMORC0CyAEqD29X93LLCTikfr.', '2019-05-04 22:51:45'),
(13, 'test', '$2y$10$ZvoYCCH38LoyZlaPXM91p.R9WOkBBaLkfcb4hBJm3Ftpxu1FViSju', '2019-05-13 17:22:43');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `kategorie`
--
ALTER TABLE `kategorie`
  ADD PRIMARY KEY (`KategoriaID`);

--
-- Indexes for table `licencie`
--
ALTER TABLE `licencie`
  ADD PRIMARY KEY (`LicencieID`);

--
-- Indexes for table `platforma`
--
ALTER TABLE `platforma`
  ADD PRIMARY KEY (`PlatformaID`);

--
-- Indexes for table `polozky`
--
ALTER TABLE `polozky`
  ADD PRIMARY KEY (`PolozkyID`),
  ADD KEY `IDLicencia` (`IDLicencia`),
  ADD KEY `IDPlatforma` (`IDPlatforma`),
  ADD KEY `IDKategoria` (`IDKategoria`);

--
-- Indexes for table `pouzivatelia`
--
ALTER TABLE `pouzivatelia`
  ADD PRIMARY KEY (`PouzivatelID`),
  ADD UNIQUE KEY `meno` (`meno`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `kategorie`
--
ALTER TABLE `kategorie`
  MODIFY `KategoriaID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `licencie`
--
ALTER TABLE `licencie`
  MODIFY `LicencieID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `platforma`
--
ALTER TABLE `platforma`
  MODIFY `PlatformaID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `polozky`
--
ALTER TABLE `polozky`
  MODIFY `PolozkyID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `pouzivatelia`
--
ALTER TABLE `pouzivatelia`
  MODIFY `PouzivatelID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `polozky`
--
ALTER TABLE `polozky`
  ADD CONSTRAINT `polozky_ibfk_1` FOREIGN KEY (`IDLicencia`) REFERENCES `licencie` (`LicencieID`),
  ADD CONSTRAINT `polozky_ibfk_2` FOREIGN KEY (`IDPlatforma`) REFERENCES `platforma` (`PlatformaID`),
  ADD CONSTRAINT `polozky_ibfk_3` FOREIGN KEY (`IDKategoria`) REFERENCES `kategorie` (`KategoriaID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
